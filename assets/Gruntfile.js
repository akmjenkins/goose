module.exports = function(grunt) {
	
	//load tasks	
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	
	/**
	 * GRUNT-SASS is FASTER than GRUNT-CONTRIB-SASS (the former uses libsass while the latter uses the ruby gem)
	 * If using Bourbon >=4, you MUST use grunt-contrib-sass. Bourbon >=4 will fail when being compiled with libsass
	 */
	//grunt.loadNpmTasks('grunt-contrib-sass'); 
	grunt.loadNpmTasks('grunt-sass');
	
	
	grunt.loadNpmTasks('grunt-cssshrink');
	grunt.loadNpmTasks('grunt-contrib-requirejs');
	grunt.loadNpmTasks('grunt-modernizr');
	grunt.loadNpmTasks('grunt-tinypng');
	grunt.loadNpmTasks('grunt-bower-task');
	grunt.loadNpmTasks('node-sprite-generator');
	grunt.loadNpmTasks('grunt-contrib-compress');
	grunt.loadNpmTasks('grunt-shell');
	
	var sizeOf = require('image-size');

	var config = {
	
		userDefaults: grunt.file.readJSON("user.defaults.json"),
	
		/**
         	 * We read in our `package.json` file so we can access the package name and version. It's already there, so
         	 * we don't repeat ourselves here.
         	 */
		pkg: grunt.file.readJSON("package.json"),
	
		/**
		 * ImageMin - TinyPNG appears to do a slightly better job on PNGs than ImageMin
		 */
		 imagemin: {
			all: {
				options: {
					
				},
				files: [{
					expand:true,
					cwd: '<%= project.dist.images %>',
					src: ['**/*.{png,jpg,gif,svg}'],
					dest: '<%= project.dist.images %>'
				}]
			}
		 },
	
		/**
		 * Project Definitions
   		 */
		project: {
			dev: {
				path: 'src',
				css: '<%= project.dev.path %>/css',
				js: '<%= project.dev.path %>/js',
				fonts: '<%= project.dev.path %>/fonts',
				images: '<%= project.dev.path %>/images',
			},
			dist: {
				path: 'bin',
				css: '<%= project.dist.path %>/css',
				js: '<%= project.dist.path %>/js',
				fonts: '<%= project.dist.path %>/fonts',
				images: '<%= project.dist.path %>/images',
			}
			
		},
		
		/**
		 * TINYPNG
		 * Run this task after all others, acts only on dist folder
		 */
		tinypng: {
			options: {	
				apiKey: '<%= userDefaults.tinypng.apiKey %>'
			},
			compress: {
				cwd: '<%= project.dist.images %>',
				expand: true, 
				src: '**/*.png', 
				dest: '<%= project.dist.images %>', 
				ext: '.png'
			},
			
			favicon: {
				cwd: '<%= project.dev.images %>',
				expand: true, 
				src: 'favicons/*.png', 
				dest: '<%= project.dev.images %>', 
				ext: '.png'
			}
		},
		
        /**
         * Minifies RJS files and makes it production ready
         * Build files are minified and encapsulated using RJS Optimizer plugin
         */
        requirejs: {
		
			options: {
				baseUrl: "<%= project.dev.js %>",
				paths:
				{
					jquery: '../../bower_components/jquery/dist/jquery',
					magnificpopup: '../../bower_components/magnific-popup/dist/jquery.magnific-popup.min'
				},
				out: '<%= project.dist.js %>/main.js',
				name: 'main'
			},
			
			dev:{
				options: {
					optimize: "none",
				}
			},
			dist: {
				options: {
					preserveLicenseComments: true,
					optimize: "uglify",
				}
			}
        },
		
		/**
		 * BOWER
		 */
		bower: {
			install: {
				options: {
					targetDir: '<%= project.dist.path %>'
				}
			}
		},

		
		/**
		 * SPRITES
		 */
		spriteGenerator: {
		
			regular: {
				src: ['<%= project.dev.images %>/sprites/regular/*.png'],
				spritePath: '<%= project.dev.images %>/sprites.png',
				stylesheetPath: 'src/css/_sprites.scss',
				stylesheetOptions: {
					spritePath: '../images/sprites.png'
				}
			},
			
			'2x': {
				src: ['<%= project.dev.images %>/sprites/@2x/*.png'],
				spritePath: '<%= project.dev.images %>/sprites@2x.png',
				stylesheetPath: 'src/css/_sprites@2x.scss',
				stylesheetOptions: {
					spritePath: '../images/sprites@2x.png',
					pixelRatio: '2'
				}
			},
			
			'3x': {
				src: ['<%= project.dev.images %>/sprites/@3x/*.png'],
				spritePath: '<%= project.dev.images %>/sprites@3x.png',
				stylesheetPath: 'src/css/_sprites@3x.scss',
				stylesheetOptions: {
					spritePath: '../images/sprites@3x.png',
					pixelRatio: '3'
				}
			}
			
		},
		
		/**
		 * COMPRESS
		 *
		 */
		compress: {
			main: {
				options: {
					archive: 'archive.zip',
				},
				files: [
					{
						expand:true,
						cwd: '../templates/',
						src: ['*.php','inc/*.*'],
						dest: 'templates'
					},
					{
						expand:true,
						src: ['./bin/**'],
						dest: 'assets'
					}
				]
			}
		},
		
		/**
 		 * UGLIFY
		 */
		uglify: {
		
			//DEV
			dev: {
				options: {
					mangle: false,
					beautify: true,
					compress: false,
				},
				
				files: [
					{
						expand: true,
						ext: '.min.js',
						cwd: '<%= project.dev.js %>/modules',
						src: [
								'**/*.js'
						],
						dest: '<%= project.dist.js %>/modules'
					},
					
					//minify all bower-dependency scripts
					{
						expand: true,
						ext: '.min.js',
						cwd: '<%= project.dist.path %>/lib/js',
						src: [
								'**/*.js'
						],
						dest: '<%= project.dist.path %>/lib/js'
					}
				]
			},
			
			//DIST
			dist: {
				options: {},
				files: [
					{
						expand: true,
						ext: '.min.js',
						cwd: '<%= project.dev.js %>/modules',
						src: [
								'**/*.js'
							],
						dest: '<%= project.dist.js %>/modules'
					},
					
					//minify all bower-dependency scripts
					{
						expand: true,
						ext: '.min.js',
						cwd: '<%= project.dist.path %>/lib/js',
						src: [
								'**/*.js'
						],
						dest: '<%= project.dist.path %>/lib/js'
					}
				]
			}
			
		},

		/**
		 * SASS
		 */
		sass: {
		
			//DEV	
			dev: {
				options: {
					style: 'expanded'
				},
				files: {
					/* destination : source */
					'<%= project.dist.css %>/style.css' : '<%= project.dev.css %>/style.scss'
				}
			},

			//DIST
			dist: {
				options: {
					style: 'compressed'
				},
				files: {
					/* destination : source */
					'<%= project.dist.css %>/style.css' : '<%= project.dev.css %>/style.scss'
				}
			}
		},
		
		/* CSS Shrink */
		cssshrink: {
			dist: {
				files: [
					{'<%= project.dist.css %>/style.css' : '<%= project.dist.css %>/style.css'},
					{
						expand: true,
						cwd: '<%= project.dist.path %>/lib/css',
						src: '**/*.css',
						dest: '<%= project.dist.path %>/lib/css'
					}
				]
			}
		},
		/** 
		 * MODERNIZR
		 */
		modernizr: {
			dist: {
				devFile: 'remote',
				outputFile: '<%= project.dist.js %>/lib/modernizr/modernizr.js',
				extra: {
					shiv: false,
					load: false,
				},
				tests: [
					'touch'
				],
				parseFiles: false,
				
				customTests: [
					'<%= project.dev.js %>/modernizr_tests/*.js'
				]
			}
		},
		 
		
		/**
		 * WATCH
		 */
		watch: {
			css: {
				files: ['<%= project.dev.path %>/**/*.scss'],
				tasks: ['copy:process_sprite_css','sass:dev']
			},
			js: {
				files: '<%= project.dev.js %>/**/*.*',
				tasks: ['copy:js','requirejs:dev']
			},
			sprites: {
				files: ['<%= project.dev.images %>/sprites/**'],
				tasks: ['sprites']
			},
			images: {
				files: ['<%= project.dev.images %>/**','!<%= project.dev.images %>/sprites/**'],
				tasks: ['clean:images','copy:images']
			}
		},
		
		/**
		 * COPY
		 */
		copy: {
			//copy all images - not in the sprites folders
			images: {
				files: [{
						expand: true,
						cwd: '<%= project.dev.images %>',
						src: [
							'**/*.png',
							'**/*.jpg',
							'**/*.gif',
							'**/*.ico',
							'**/*.svg',
							'!sprites/**'
						],
						dest: '<%= project.dist.images %>'
				}]
			},
			
			//copy all fonts - not in an svg folder
			fonts: {
				files: [{
					expand: true,
					cwd: '<%= project.dev.fonts %>',
					src: ['**/**','!**/svg/**'],
					dest: '<%= project.dist.fonts %>'
				}]
			},
			
			//copy all js libraries and the scripts folder
			js: {
				files: [{
					expand: true,
					cwd: '<%= project.dev.js %>/modules',
					src: [
						'**/*.*'	//copy any file
						],
					dest: '<%= project.dist.js %>/modules'
				}]
			},
			
			//specific task designed to take the generated sprites.scss and sprites@2x.scss
			//files and convert them into  the format that this project is actually using
			process_sprite_css: {
				options: {
					process:function(content,srcpath) {
						
						var 
							dimensions,
							newContent = '';
							spritePath = config.project.dev.path+'/images/sprites.png',
							lines = content.split('\n');
							lines.splice(-6); //remove the bottom of the file - not needed
							
							
							//start the new content with a '
							newContent += "'"+lines.join("\n");
							
							//three regexes
							//replace all 'px' with nothing
							newContent = newContent.replace(new RegExp('px','g'),'');
							
							//replace all '\n' with ,\n'
							newContent = newContent.replace(new RegExp('\n','g'),",\n'");
							
							//replace all ' =' with '
							newContent = newContent.replace(new RegExp(' =','g'),"'");
							
							//start newContent with scss list definition
							newContent = '$spriteList: (\n'+newContent;
							//end newContent scss list definition
							newContent += '\n);\n';
							
							//add in variables for use with retina
							if(grunt.file.exists(spritePath)) {
								dimensions = sizeOf(spritePath);
							} else {
								dimensions = {
									width:0,
									height:0
								}
							}
							
							newContent += '$spriteSheetSizeWidth: '+dimensions.width+'px;\n';
							newContent += '$spriteSheetSizeHeight: '+dimensions.height+'px;\n';
							
							//remove _sprite.scss, _sprites@2x.scss, _sprites@3x.scss
							grunt.file.delete('src/css/_sprites.scss');
							grunt.file.delete('src/css/_sprites@2x.scss');
							grunt.file.delete('src/css/_sprites@3x.scss');
							
						return newContent;
					}
				},
				files: [
					{
						src: '<%= project.dev.css %>/_sprites.scss',
						dest: '<%= project.dev.css %>/_processed_sprites.scss'
					}
				]
			}
			
		},
		
		/**
		 * CLEAN
		 */
		clean: {
			//clean fonts/images directories
			images: ['<%= project.dist.images %>'],
			css: ['<%= project.dist.css %>'],
			js: ['<%= project.dist.js %>/*'],
			bower: ['<%= project.dist.path %>/lib/*'],
			fonts: ['<%= project.dist.fonts %>']
		},
		
		/**
		 * SHELL - DO ANYTHING
		 */
		shell: {
		
			//requires imagemagick to be installed
			favicon: {
				command: [
					'grunt tinypng:favicon',
					'cd <%= project.dev.images %>/favicons',
					'convert favicon-16.png favicon-24.png favicon-32.png favicon-48.png favicon-64.png favicon.ico',
					'grunt copy:images'
				].join('&&')
			}
		}

	};
	
	//Configuration
	grunt.initConfig(config);
	
	
	//////////////////////////////////////////////////////////////////////
	//	TASKS
	//////////////////////////////////////////////////////////////////////
	
	grunt.registerTask("clean:all",[
		'clean'
	]);
	
	//Default
	grunt.registerTask("default",[
		'dev'
	]);
	
	//Dev
	grunt.registerTask("dev",[
		'copy',
		'sass:dev',
		'modernizr',
		'clean:bower',
		'bower',
		'cssshrink',
		'uglify:dev',
		'requirejs:dev'
	]);
	
	//Sprites
	grunt.registerTask("sprites",[
		'spriteGenerator',
		'copy:images',
		'copy:process_sprite_css',
		'sass:dev'
	]);
	
	grunt.registerTask("dev-watch",[
		'dev',
		'watch'
	]);
	
	//Prod
	grunt.registerTask("prod",[
		'clean',
		'copy',
		'bower',
		'sass:dist',
		'cssshrink',
		'modernizr',
		'uglify:dist',
		'requirejs',
		//'tinypng:compress'
	]);
	
	//favicon
	grunt.registerTask("favicon",[
		'shell:favicon'
	]);
	
	
	//compress
	grunt.registerTask("compress-dev",[
		'dev',
		'compress'
	]);
	
	grunt.registerTask("compress-prod",[
		'prod',
		'compress'
	]);

};
