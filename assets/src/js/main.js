//load all required scripts
requirejs(
	[
		//include jQuery from here - no extra HTTP request
		'jquery',
	
		'scripts/anchors.external.popup',
		'scripts/gmap',
		'scripts/magnific.popup',
		'scripts/standard.accordion',
		'scripts/responsive.video',
		'scripts/custom.select',
		'scripts/aspect.ratio',
		'scripts/lazy.images',

		'scripts/nav'
		
	],
	function() {
	
		
	
		var $document = $(document);
			
		$document
			.on('tabChanged',function(e,el) {
				$document.trigger('updateTemplate');
			});
		
		$('body.home .hero').particleground({
			lineColor: '#ffffff',
			dotColor: '#ffffff',
			density: 5000,
			minSpeedX: 0.1,
			minSpeedY: 0.1,
			parallaxMultiplier: 10
		});
		
		
});