<!doctype html>
<html lang="en">

	<head>
		<title>Goose Pond Country Properties</title>
		<meta charset="utf-8">
		
		<!-- Lato -->
		<link href="//fonts.googleapis.com/css?family=Lato:400" rel="stylesheet">
		
		<!-- favicons -->
		<link rel="icon" type="image/x-icon"  href="../assets/bin/images/favicons/favicon.ico">
		<link rel="icon" type="image/png"  href="../assets/bin/images/favicons/favicon-32.png">
		<link rel="icon" href="../assets/bin/images/favicons/favicon-32.png" sizes="32x32">
		<link rel="apple-touch-icon-precomposed" sizes="152x152" href="../assets/bin/images/favicons/favicon-152.png">
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/bin/images/favicons/favicon-144.png">
		<link rel="apple-touch-icon-precomposed" sizes="120x120" href="../assets/bin/images/favicons/favicon-120.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/bin/images/favicons/favicon-114.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/bin/images/favicons/favicon-144.png">
		<link rel="apple-touch-icon-precomposed" href="../assets/bin/images/favicons/favicon-114.png">	
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="../assets/bin/images/favicons/favicon-144.png">
		
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0">
		
		<link rel="stylesheet" href="../assets/bin/css/style.css?<?php echo time(); ?>">		
		
	</head>
	<body>
	
		<div class="page-wrapper">	
			<header>
					
					<a href="#" class="header-logo">
						<img src="../assets/bin/images/goose-pond-country-properties.svg" alt="Goose Pond Country Properties">
					</a>
					
			</header>
			
			<div class="hero">
				
				<div class="hgroup">
					<h1>New Website Coming Soon</h1>
					<h4>Until then, please visit our current website for more information on our properties</h4>
				</div><!-- .hgroup -->
				
				<a href="#" class="button">Visit Our Website</a>
				
			</div><!-- .hero -->
			

			<footer>
			
				<div class="sw">
					<div class="footer-blocks">
						<div class="footer-block main-footer-block">
							
							<div class="hgroup">
								<h2 class="uc">Now Selling</h2>
								<h4>Phase 7</h4>
							</div><!-- .hgroup -->
							
							<span class="lots">
								<span class="circle-button">20</span>
								Lots Available
							</span><!-- .lots -->
							
						</div><!-- .footer-block -->
						<div class="footer-block">
							
							<span class="t-fa-abs fa-envelope-o circle-button sm">E-mail</span>
							<a href="#">info@goosepond.ca</a>
							
						</div><!-- .footer-block -->
						<div class="footer-block">
						
							<span class="t-fa-abs fa-phone circle-button sm">Phone</span>
							<a href="#">1 709 699 7663</a>
							
						</div><!-- .footer-block -->
					</div><!-- .footer-blocks -->
				</div><!-- .sw -->
				
				<div class="copyright">
					<div class="sw">
						<ul>
							<li>Copyright &copy; <?php echo date('Y'); ?> <a href="/">Goose Pond Properties</a></li>
						</ul>
						
						<a href="http://jac.co" rel="external" title="JAC. We Create." id="jac"><img src="../assets/bin/images/jac-logo.svg" alt="JAC Logo."></a>
					</div><!-- .sw -->
				</div><!-- .copyright -->
			</footer><!-- .footer -->
		
		</div><!-- .page-wrapper -->
		
	</body>
</html>